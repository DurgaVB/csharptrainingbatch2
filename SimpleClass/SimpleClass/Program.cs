﻿using System;

namespace SimpleClass
{
    class Program
    {
        static void Main(string[] args)
        {
          
            //Employee empObj = new Employee();
            //empObj.EmpId = 1;
            //empObj.EmpId = 2;

            //empObj.EmpName = "ABC";
            
            //Console.WriteLine(empObj.EmpId);

            //Console.WriteLine(empObj.EmpName);
            //Console.ReadLine();

            //empObj.EmpId = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine("Emp Id:" + empObj.EmpId);
            //empObj.EmpName = Console.ReadLine();
            //Console.WriteLine("Emp Name:" + empObj.EmpName);

            Employee empObjNew = new Employee(1,"JKL");
            Console.WriteLine("EmpID :" + empObjNew.EmpId + ", Emp Name:" + empObjNew.EmpName);

            string noOfEmp = empObjNew.NoOfEmp(2, "abc");
            Console.WriteLine(noOfEmp);


            Employee EmpObjLatest;// = new Employee();
            empObjNew.EmpId = 4;
            empObjNew.EmpName = "XYZ";
            Console.WriteLine("EmpID :" + empObjNew.EmpId + ", Emp Name:" + empObjNew.EmpName);
           
            EmpObjLatest = empObjNew;

            Console.WriteLine("EmpID :" + EmpObjLatest.EmpId + ", Emp Name:" + EmpObjLatest.EmpName);
            EmpObjLatest.EmpId = 12;
            EmpObjLatest.EmpName = "SD";
            Console.WriteLine("EmpObjLatest" +  "EmpID :" + EmpObjLatest.EmpId + ", Emp Name:" + EmpObjLatest.EmpName);
            Console.WriteLine("empObjNew" + "EmpID :" + empObjNew.EmpId + ", Emp Name:" + empObjNew.EmpName);


            ////Console.WriteLine(EmpObjLatest.EmpId + ","+  EmpObjLatest.EmpName);

            Console.ReadLine();
        }
    }
}