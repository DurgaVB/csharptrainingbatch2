﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutParameter
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, y;
            int c = 2, d = 4;
            Multiplication(c,d,out x, out y);

            Console.WriteLine("x Value: {0}", x);

            Console.WriteLine("y Value: {0}", y);

            Console.WriteLine("Press Enter Key to Exit..");

            Console.ReadLine();

        }

        public static void Multiplication(int c, int d,out int a, out int b)

        {

            a = c;

            b = d;

            a *= a;

            b *= b;

        }
    }
}
