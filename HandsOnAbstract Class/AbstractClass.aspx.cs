﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


//Abstract class is an incomplete class or special class we can't instantiated. We can us this Abstract class as a Base Class.
//Abstract method must implement on the non-Abstract class by using override keyword.
//After override abstract method in the non-Abstract class. We can derived this class into antoher class and again we can override same abstract method with it.

//Features:
//1. An abstract calss can inherit from a class and one or more interfaces.
//2. An abstract class can implement code with non-Abstract methods.
//3. Abstract class can have modifiers for methods,properties etc.,
//4. Abstract class can have constant and fields.
//5. An abstract class can implement a property.
//6. An abstract class can have constructors or destructors.
//7. An abstract class cannot be inherited from by structures.
//8. An abstract class cannot support multiple inheritance.



#region An abstract calss can inherit from a class and one or more interfaces.
interface IVendorTransDetails
{
    void getVendorID();
}

interface IClaimsTracker
{
    void getSeqID();
}

class ClaimsMaster
{
    string getDCNNO()
    {
        return "PC20100308A00005";
    }
}

abstract class Abstract : ClaimsMaster, IClaimsTracker, IVendorTransDetails
{
    //Here we should implement modifiers oterwise it throws complie-time error
    public void getVendorID()
    {
        int s = new int();
        s = 001;
        Console.Write(s);
    }


    public void getSeqID()
    {
        int SeqID = new int();
        SeqID = 001;
        Console.Write(SeqID);
    }
}

#endregion

#region An abstract class can implement code with non-Abstract methods.

abstract class NonAbstractMethod
{
    //It is a Non-abstract method we should implement code into the non-abstract method on the class. 
    public string getDcn()
    {
        return "PS20100301A0012";
    }

    public abstract void getSeqID();

}

class Utilize : NonAbstractMethod
{
    public override void getSeqID()
    {
    }


}

#endregion

#region Abstract class can have modifiers for methods,properties and An abstract class can implement a property

public abstract class abstractModifier
{
    private int id;

    public int ID
    {
        get { return id; }
        set { id = value; }
    }

    internal abstract void Add();
}

#endregion

#region Abstract class can have constant and fields

public abstract class ConstantFields
{
    public int no;
    private const int id = 10;
}

#endregion

#region An abstract class can have constructors or destructors
abstract class ConsDes
{
    ConsDes()
    {
    }

    ~ConsDes()
    {
    }
}
#endregion

#region An abstract class cannot be inherited from by structures

public struct test
{
}

//We can't inheritance the struct class on the abstract class
abstract class NotInheritanceStruct
{
}

#endregion

#region An abstract class cannot support multiple inheritance
class A
{
}

class B : A
{
}

abstract class Container : B //But we can't iherit like this : A,B
{
}

#endregion

public partial class Abstract_Class_AbstractClass : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


    }
}
