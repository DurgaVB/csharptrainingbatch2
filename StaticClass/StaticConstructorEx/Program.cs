﻿using System;

namespace StaticConstructorEx
{
    class User
    {
        // Static Constructor
        static User()
        {
            Console.WriteLine("I am Static Constructor");
        }
        
        // Default Constructor
        public User()
        {
            Console.WriteLine("I am Default Constructor");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Both Static and Default constructors will invoke for first instance
            User user = new User();
            // Only Default constructor will invoke
            User user1 = new User();
            Console.WriteLine("\nPress Enter Key to Exit..");
            Console.ReadLine();
        }
    }
}
