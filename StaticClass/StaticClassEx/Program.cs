﻿using System;

namespace StaticClassEx
{
     class User
    {
        // Static Variables
        public static string name;
        public static string location;
        //Non Static Variable
        public int age;
        // Non Static Method
        public void Details()
        {
            Console.WriteLine("Non Static Method");
        }

        // Static Method
        public static void Details1()
        {
            Console.WriteLine("Static Method");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            User u = new User();
            u.age = 32;
            u.Details();
            User.name = "Suresh Dasari";
            User.location = "Hyderabad";
            Console.WriteLine("Name: {0}, Location: {1}, Age: {2}", User.name, User.location, u.age);
            User.Details1();
            Console.WriteLine("\nPress Enter Key to Exit..");
            Console.ReadLine();
        }
    }
}
