﻿using System;

namespace Interface
{
    interface IEngine
    {
        void Start();
        int Add(int x, int y);
    }
    public class Car : IEngine
    {
        public int Add(int x, int y)
        {
            int z = x + y;
            return z;
        }

        public void Start()
        {
            Console.Write("Car Started.");
        }
    }





    public class Truck : IEngine
    {
        public int Add(int x, int y)
        {
            
           return  x + y;
        }

        public void Start()
        {
            Console.Write("Truck Started.");
        }
    }

    public class Tricyclic : IEngine
    {
        public int Add(int x, int y)
        {
            return x + y;
        }

        public void Start()
        {
            Console.Write("Tricyclic Started.");
        }
    }
    class Program
    {
        static void StartEngine(IEngine obj)
        {
            Console.WriteLine();
            obj.Start();
        }
        static void Main(string[] args)
        {
            //var car = new Car();
            //Truck truck = new Truck();
            //var tricyclic = new Tricyclic();

            //Console.WriteLine("Below are the Implementations of Start method for different vehicle");

            //Console.WriteLine();
            //car.Start();
            //Console.WriteLine(car.Add(1, 3));
           
            //Console.WriteLine();
            //truck.Start();
            //Console.WriteLine();
            //tricyclic.Start();
            //Console.WriteLine();

           // Console.ReadLine();


            Console.WriteLine("Below are the Implementations of Start method for different vehicle");

            StartEngine(new Car());
            StartEngine(new Truck());
            StartEngine(new Tricyclic());

            Console.ReadLine();


        }
    }
}
