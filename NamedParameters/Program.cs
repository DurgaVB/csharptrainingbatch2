﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NamedParameters
{
    class Program
    {
        static void Main(string[] args)

        {

            GetDetails(1, "Suresh", "Hyderabad");

            GetDetails(name: "Rohini", id: 2, location: "Guntur");

            GetDetails(3, "Trishi", location: "Guntur");

            Console.ReadLine();

        }

        public static void GetDetails(int id, string name, string location)

        {

            Console.WriteLine("Id:{0}", id);

            Console.WriteLine("Name:{0}", name);

            Console.WriteLine("Location:{0}", location);

        }
    }
}
