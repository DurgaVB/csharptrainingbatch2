﻿using System;

namespace Constructor
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee empObj = new Employee(1,"ABC");
            
            Console.WriteLine("Id:" + empObj.EmpId);
            Console.WriteLine("Name:" + empObj.EmpName);
            Console.ReadLine();

        }
    }
}
