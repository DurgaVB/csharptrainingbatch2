﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Constructor
{
   public class Employee
    {
        public int EmpId { get; set; }
        public string  EmpName { get; set; }

        public Employee(int Id, string Name)
        {
            EmpId = Id;
            EmpName = Name;
        }
    }
}
