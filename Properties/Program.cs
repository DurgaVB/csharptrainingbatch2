﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{

    class User

    {

        private string location;

        private string name = "Suresh Dasari";
        
        public int MyProperty { get; set; }
        public string Location

        {

            get { return location; }

            set { location = value; }

        }

        public string Name

        {

            get

            {

                return name.ToUpper();

            }

            set

            {

            //    if (value == "Suresh Dasari")

                    name = value;

            }

        }

    }

    class Program

    {

        static void Main(string[] args)

        {

            User u = new User();

            // set accessor will invoke

            u.Name = "Rohini";
            User u1 = new User();
          //  u1.Name = u.Name;
            // set accessor will invoke

            u.Location = "Hyderabad";

            // get accessor will invoke

            Console.WriteLine("Name: " + u.Name);
            Console.WriteLine("Name: " + u1.Name);

            // get accessor will invoke

            Console.WriteLine("Location: " + u.Location);

            Console.WriteLine("\nPress Enter Key to Exit..");

            Console.ReadLine();

        }

    }
}
