﻿using System;

namespace Properties
{
    class User

    {
        /// <summary>
        /// Syntax :  AccessModifier Datatype variableName { get; set;}
        ///   public int MyProperty { get; set; }
        /// </summary>

        public int add(int x, int y)
        {
            int c = x + y;
            return c;
        }

      //  private string location;

       // private string name = "Suresh Dasari";
      
        public string Location

        //{

        //    get { return location; }

        //    set { location = value; }

        //}

        public string Name

        {

            get

            {
                //string abc = name.ToUpper();

                return Name;
                    //name.ToUpper();

            }

            set

            {

                if (value == "Suresh")

                    Name = value;

            }

        }

    }

    class Program

    {

        public static int MyProperty { get; set; }
        static void Main(string[] args)

        {
            MyProperty = 1;

             User u = new User();
          //  u.add
            // set accessor will invoke

            u.Name = "Rohini";

            // set accessor will invoke

            u.Location = "Hyderabad";

            // get accessor will invoke

            Console.WriteLine("Name: " + u.Name);

            // get accessor will invoke

           // Console.WriteLine("Location: " + u.Location);

            Console.WriteLine("\nPress Enter Key to Exit..");

            Console.ReadLine();

        }

    }
}
