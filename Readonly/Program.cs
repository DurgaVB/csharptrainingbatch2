﻿using System;

namespace Readonly
{
    class User

    {

        // Initialize Read Only Fields

        public readonly string name = "Suresh Dasari";

        public readonly string location;

        public readonly int age;

        public User()

        {

            location = "Hyderabad";

            age = 32;

        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            User u = new User();

            // This will throw compile time error

            //u.name = "Rohini Alavala";

            Console.WriteLine("Name: {0}", u.name);

            Console.WriteLine("Location: {0}", u.location);

            Console.WriteLine("Age: {0}", u.age);

            Console.WriteLine("\nPress Enter Key to Exit..");

            Console.ReadLine();
        }
    }
}
