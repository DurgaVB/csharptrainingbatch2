﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodOverloading
{
    public class Calculate

    {

        public void AddNumbers(int a, int b)

        {

            Console.WriteLine("a + b = {0}", a + b);

        }

        public void AddNumbers(int a, int b, int c)

        {

            Console.WriteLine("a + b + c = {0}", a + b + c);

        }
        public void AddNumbers(float a, float b)

        {

            Console.WriteLine("a + b  = {0}", a + b );

        }
        public void AddNumbers(double a, double b)

        {

            Console.WriteLine("a + b + c = {0}", a + b);

        }
        public void AddNumbers(string a, string b)

        {

            Console.WriteLine("a + b  = {0}", a + b);

        }
        public void AddNumbers(string a, string b, string c)

        {

            Console.WriteLine("a + b + c  = {0}", a + b+c );

        }
    }

    class Program

    {

        static void Main(string[] args)

        {

            Calculate c = new Calculate();

            c.AddNumbers(b:1,a:2);

            c.AddNumbers(1, 2, 3);
            c.AddNumbers(1.2f, 2.5f);
            c.AddNumbers(2.5, 4.0);
            c.AddNumbers("a", "b", "C");
            Console.WriteLine("\nPress Enter Key to Exit..");

            Console.ReadLine();

        }

    }
}
