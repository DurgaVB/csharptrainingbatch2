﻿using System;

namespace PropertySample
{
    class Program
    {
        private static string lastName = "ABC";
        public static string  Name {

            get {
                return lastName.ToLower();
            }
            set
            {
                lastName = value;
            }
      
        }


        static void Main(string[] args)
        {
            Name = "XYZ";
            Console.WriteLine("Name:" + Name);
            Console.ReadLine();
        }
    }
}
