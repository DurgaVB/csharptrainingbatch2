﻿using System;

namespace Constant
{
    class Program
    {


        static void Main(string[] args)

        {
            ///
            /// Syntax : ConstantKeyword(const) Datatype variableName = value;
            ///

          

            // Constant variables

            const string name = "Suresh Dasari";

            const string location = "Hyderabad";

            const int age = 32;

            // This will throw compile time error
         

            //name = "Rohini Alavala";

            Console.WriteLine("Name: {0}", name);

            Console.WriteLine("Location: {0}", location);

            Console.WriteLine("Age: {0}", age);

            Console.WriteLine("\nPress Enter Key to Exit..");

            Console.ReadLine();

        }

    }

}
